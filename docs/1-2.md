![](assets/4b0e0cf18781f1d839608e765a9b1ff221808628.jpg)

---

# Datenbank und Models

Ein Django-Model stellt das Schema der DB dar und ist eine Python-Klasse: `models.py`

In dieser Klasse können wir eine Tabelle mit Attributen anlegen.

Mit der `Database Abstraction API` können wir DB-Objekte erstellen, updaten und löschen.

Standard-DB ist SQLite, es ist aber auch möglich andere DBMS zu koppeln - auch nicht relationale wie z.B. MongoDB

Die Anbindung der DB an Django wird in `settings.py` verwaltet:

![](assets/2024-03-08-17-23-05-image.png)

Konkretes Model für die Tabelle `Film`:

```py
from django.db import models

# Die Python-Klasse Film steht für eine Tabelle in der Datenbank
class Film(models.Model):
    film_name = models.CharField(max_length=200)
    film_beschr = models.CharField(max_length=200)
    film_bewertung = models.IntegerField()
```

Um die Tabelle in der DB anzulegen, muss die App zuerst in `settings.py` registriert werden. Um an die Bezeichnung unserer App zu kommen, öffnen wir `apps.py`:

```py
from django.apps import AppConfig


class FilmsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'films'
```

In `settings.py` registrieren wir nun unsere App:

![](assets/2024-03-08-17-43-13-image.png){width="450"}

!!! info

    Zeilen 35-40 sind Standard-Apps, welche mit der Projektinstallation mitkommen.
    Aber auch für diese müssen zuerst Tabellen in der DB angelegt werden.

## Model registrieren

Zuerst muss das Model selbst bei Django registriert werden. Mit dem Parameter `makemigrations` untersucht Django `models.py` auf Änderungen und Migriert die Model(s).

```bash
python manage.py makemigrations films
```

![](assets/2024-03-08-17-46-04-image.png)

## Tabellen erzeugen

Zuerst migrieren wir das Model in die DB:

```bash
python manage.py sqlmigrate films 0001
```

![](assets/2024-03-08-17-51-26-image.png)

Wenn wir mit den SQL-Commands zufrieden sind, erzeugen wir schlussendlich die Tabelle:

```bash
python manage.py migrate
```

Der Befehl erzeugt neben unserer Tabelle noch weitere (Apps Zeilen 35-40 oben).

## Daten der Relation hinzufügen

### Variante 1: Python InteractiveConsole

```bash
python manage.py shell
```

![](assets/2024-03-09-08-18-35-image.png)

DB-Model (Schema) von App `films` importieren

```bash
from films.models import Film
```

Daten anzeigen

```bash
Film.objects.all()
```

![](assets/2024-03-09-08-26-56-image.png)

Array ist leer

Daten als Objekt hinzufügen:

![](assets/2024-03-09-08-29-48-image.png)

```bash
a = Film(film_name="Dune 2", film_beschr ="Paul Atreides verbündet sich mit Chani und den Fremen, um sich an den Verschwörern zu rächen, die seine Familie umgebracht haben. Infolgedessen muss er sich zwischen der Liebe seines Lebens und dem Schicksal des Universums entscheiden.", film_bewertung=5)
```

Objekt der Tabelle als Datensatz hinzufügen (speichern):

```bash
a.save()
```

![](assets/2024-03-09-08-34-48-image.png)

Unique ID (Primärschlüssel) abgreifen:

```bash
a.id
```

```bash
a.pk
```

![](assets/2024-03-09-08-36-22-image.png)

Nach Hinzufügen eines weiteren Objekts ist die Objektstruktur im QuerySet gut erkennbar:
![](assets/2024-03-09-08-43-35-image.png)

Das Resultat ist leider nicht sehr aussagekräftig. Besser wäre es, wenn der Filmtitel angezeigt wird. Das können wir im Model festlegen, indem wir die Funktion `str` hinzufügen:

```py
class Film(models.Model):

    def __str__(self):
        return self.film_name

    film_name = models.CharField(max_length=200)
```

Damit die Änderungen im Model auch berücksichtigt werden, muss die Shell zuerst mit `exit()` geschlossen und anschliessend nochmals geöffnet werden

```bash
exit()
python manage.py shell
from films.models import Film
Film.objects.all()
```

![](assets/2024-03-09-08-59-54-image.png)

### Variante 2: Admin-Panel

Bequemer und einfacher sind die Daten mit dem Admin-Panel von Django verwaltbar.

**Super-User anlegen**

```bash
python manage.py createsuperuser 
```

Auf `localhost:portnr/admin` kann man sich nun mit als Super-User anmelden![](assets/2024-03-09-09-29-37-image.png){width="400"}

![](assets/2024-03-09-09-32-49-image.png){width="400"}

Die Datenbank wird noch nicht angezeigt. Dazu öffnen wir `admin.py` in unserem App-Ordner und koppeln unser Model:

```py
from django.contrib import admin
from .models import Film

# Register your models here.
admin.site.register(Film)
```

![](assets/2024-03-09-09-37-25-image.png){width="400"}

![](assets/2024-03-09-09-39-03-image.png){width="400"}

## Daten aus DB holen und anzeigen

Wir werden nun die Film-Objekte aus der Datenbank holen und zeigen sie in unserer App (Website) an.

Wir öffnen `views.py` und schreiben unsere Request-Funktion um:

```py
from .models import Film

# Create your views here.
def index(request):
    film_liste = Film.objects.all()
    return HttpResponse(film_liste)
```

![](assets/2024-03-09-09-53-54-image.png){width="400"}
