![ksb_logo_klein.jpg](assets/1133a42db2ce6f790277d0156a509c8d59ac7b1e.jpg)

---

# M223 Multi-User-Applikation objektorientiert realisieren

Herzlich Willkommen auf den Doku-Seiten zum Modul

==Viel Erfolg im Modul!==

![](https://www.kaggle.com/static/images/home/logged-out/hero-illo.png)

## Application Engineering

!!! Info "Kompetenz"

    Multi-User-Applikation objektorientiert entwerfen, erforderliche Datenbankanpassungen vornehmen und Applikation implementieren, testen und dokumentieren.

## Handlungsziele und handlungsnotwendige Kenntnisse

??? success "Einschätzen, ob eine Datenbank die Anforderungen der Multi-User-Fähigkeit erfüllt und allfällige Anpassungen dokumentieren."

    1. Kennt Anforderungen an das Datenbankmanagement-System bezüglich Multi-User-Fähigkeit.
    
    2. Kennt Aspekte bei der Datenmodellierung, welche die Multi-User-Fähigkeit ermöglichen.

??? success "Applikation entwerfen und mittels Transaktionen Multi-User-Fähigkeit sicherstellen."

    1. Kennt die prinzipiellen Unterschiede zwischen Geschäftsobjektmodell und relationalem Datenmodell .
    
    2. Kennt wichtige Architekturvarianten und -konzepte (Client/Server, Multi-Tier, Middleware, Framework, Klassenbibliothek).
    
    3. Kennt die Umsetzung einer objekt-relationalen Abbildung eines Geschäftsobjektmodells und deren Spezifikation mittels UML (Klassendiagramm, Sequenzdiagramm).

??? success "User Interfaces, Datenbankanpassungen und Transaktionen implementieren."

    1. Kennt spezifische Elemente für die Umsetzung von Multi-User-fähigen Benutzerschnittstellen (z.B. Profil, unterschiedliche Benutzersichten, Berechtigungskonzept, usw.).
    
    2. Kennt Möglichkeiten ein mehrbenutzer-fähiges Rechtemanagement zu implementieren.
    
    3. Kennt Möglichkeiten um Transaktionen im DBMS sicherzustellen.
    
    4. Kennt Möglichkeiten um Transaktionen in der Applikation zu implementieren.
    
    5. Kennt relevante Techniken für die Implementation einer Persistenzschicht.

??? success "Testspezifikationen für funktionale und nicht-funktionale Aspekte der Multi-Userfähigkeit definieren, Applikation testen und Tests protokollieren."

    1. Kennt relevante Aspekte, welche bei der Testspezifikation einer Muli-User-Applikation zu berücksichtigen sind.
    
    2. Kennt ein Vorgehen, um nicht-funktionale Anforderungen zu testen.

??? success "Transaktionen dokumentieren und dabei auf Wartbarkeit und Nachvollziehbarkeit achten."

    1. Kennt Möglichkeiten zur Dokumentation von Transationen um DBMS und in der Applikation.