![](assets/4b0e0cf18781f1d839608e765a9b1ff221808628.jpg)

---

# Userverwaltung

Für die Userverwaltung erstellen wir eine eigene App `users` .

```bash
django-admin startapp users
```

![](assets/2024-03-13-10-14-12-image.png)

!!! info "App registrieren"

    Wenn wir eine neue App dem Projekt hinzugefügt haben, müssen wir sie in `settings.py` registrieren.

## ![](assets/2024-03-13-10-17-53-image.png)

## Registrierung User

Die User sollen sich mit Email-Adresse und Passwort authentifizieren. Wir ziehen hier vollen Nutzen aus dem Django-Framework und greifen auf mehrere Komponenten zu.

Wir starten mit `UserCreationForm`  und erzeugen eine neue `View "register"`.

`UserCreationForm` stellt uns eine fertige Form zur Registrierung zur Verfügung.

```py
from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm
# Create your views here.


def register(request):
    form = UserCreationForm()
    return render(request, 'users/register.html',{'form':form})
```

![](assets/2024-03-13-10-25-10-image.png)

```html
<form method="POST">
  {% csrf_token %} 
  {{ form }}
  <button type="submit">Registrieren</button>
</form>
```

!!! note "Verständnis"

    In der View `register` erzeugen wir ein Formular, welches auf der `UserCreationForm`-Komponenten von Django aufbaut. Wir lassen dieses Formular in register.html rendern. Das Formular übergeben wir als Kontext (`form`-Objekt).

Wir erzeugen hier den URL-Pfad nicht in der App `users`, sondern in der main-urls von `fav-films`:

![](assets/2024-03-13-10-35-58-image.png)

Wenn wir nun den Pfad `register` aufrufen, wird durch den path die `view.py` in der `users`-App aufgerufen.

![](assets/2024-03-13-10-35-10-image.png){width="400"}

### Verarbeitung Formulardaten

Unser Formular wird nun gerendert, übermittelt aber noch keine Daten und gibt auch keine Fehlermeldungen (z.B. Username schon vergeben, Password-Mismatch, u.ä.) aus.

Unsere Formulardaten werden in `register.html` mit der `POST`-Methode übermittelt.

Die Funktion `register` wird durch Eingabe der URL aufgerufen. 

!!! info "Verständnis-Booster"

    Durch Klick auf den Button `Registrieren` wird die Funktion erneut aufgerufen!

Mit `if request.method =="POST":` können wir die Formulardaten abgreifen, welche durch Klick auf den Button mit `POST` übermittelt werden. Ist die Bedingung erfüllt (Formulardaten werden übermittelt), dann validieren wir und leiten den User zur `index`-Seite weiter. Dort soll eine Bestätigungs-Message angezeigt werden.

```py
from django.contrib import messages
from django.shortcuts import redirect, render

def register(request):
    if request.method =='POST':
        # Das Formular enthält nun Daten, in form speichern
        form = UserCreationForm(request.POST)
        # Validieren, wenn valid dann username extrahieren
        # und personalisierte Message ausgeben
        if form.is_valid():
            username = form.cleaned_data.get('username')
            messages.success(request, f'Willkommen {username}! Dein Account wurde erstellt.')
            # Hinweis: Zum Testen der Form und der Message lassen wir form.save() vorläufig
            # noch weg
        return redirect('films:index')
    # request nicht POST
    else:
        form = UserCreationForm()

    return render(request, 'users/register.html',{'form':form})
```

Die Validierung der Daten und die dazugehörigen Hinweise sollten nun korrekt angezeigt werden. Sind die Daten korrekt wird der User nun zur `index`-Seite weitergeleitet. 

Dort soll nun die Bestätigungs-Message angezeigt werden. Wir entscheiden uns sie ==unter der Nav== (`base.html`) einzublenden. 

Dazu greifen wir auf den Messages-Stack zu und zeigen sie mit ein wenig Bootstrap an:

```django
{% if messages %} 
      {% for message in messages %}
        <div class="alert alert-{{ message.tags }}" role="alert">{{ message }}</div>
      {% endfor %} 
{% endif %}
```

---

Wir müssen weiter sicherstellen, dass der Nutzername immer unique ist. 
Glücklicherweise kümmert sich Django standardmässig mit der `isValid()`-Funktion und wir müssen das nicht selbst validieren.

![](assets/2024-03-13-14-10-27-image.png)

Somit können wir die Formdaten mit `form.save()` speichern:

```py
 if form.is_valid():
            username = form.cleaned_data.get('username')
            messages.success(request, f'Willkommen {username}! Du bist nun eingeloggt.')
            form.save()
            return redirect('login')
```

| Register                                  | Backend                                   |
| ----------------------------------------- | ----------------------------------------- |
| ![](assets/2024-03-13-14-10-08-image.png) | ![](assets/2024-03-20-10-21-25-image.png) |

### Email ergänzen

Wir möchten zusätzlich zu Username & Passwort auch die Email-Adressen der User verwalten.

Wie bekommen wir jetzt ein zusätzliches Feld zur Django-Standardform, welche wir mit `UserCreationForm()` erzeugt haben? `UserCreationForm` können wir nicht einfach abändern. Wir können aber unser eigenes Formular erstellen, welches die Methoden von `UserCreationForm` erbt.

!!! note "Vererben und Erweitern"

    In Python können wir von anderen Klassen erben, indem wir sie als Parameter unsererer Klasse mitgeben. So können wir in einem selbst erzeugten Formular die Grundfunktionalität erben, überschreiben oder ergänzen.

Wir erzeugen ein neues File in der `users`-App: `forms.py`

Dieses soll in Folge alle User-Formulare verwalten.

```py
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
#Vererbung
class RegisterForm(UserCreationForm):
    # Standardform um Email-Feld erweitern
    email = forms.EmailField()
```

In einer Meta-Klasse legen wir fest, wie genau wir die Parent-Klasse erweitern möchten. Die Meta-Klasse verwaltet somit Informationen über `RegisterForm` welche eine Instanz von `UserCreationForm` ist.

```py
class RegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        # Formular RegisterForm mit User verknüpfen 
        # Django weiss dann, dass beim Senden dieses Formulars ein User
        # erstellt/aktualisiert werden soll
        model = User 
        fields = ['username', 'email', 'password1','password2']
```

Nun müssen wir nur noch unser altes Formular gegen dieses in `views.py` austauschen.

```py
form = RegisterForm(request.POST)
```

![](assets/2024-03-13-14-33-34-image.png)

 Admin-Panel:

![](assets/2024-03-20-10-38-00-image.png)

## User einloggen

Auch hier unterstützt uns das Django-Framework kräftig und stellt uns einen In-Build-View zur Verfügung: `LoginView`
Dieser View ist ==klassenbasiert== und erfordert den Methodenaufruf  `.as_view()`.

```py
# urls.py fav_films ergänzen
from django.contrib.auth import views as authentication_views

# -----

path('login/', authentication_views.LoginView.as_view(template_name='users/login.html'), name='login'),
path('logout/', authentication_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),
```

Um die View brauchen wir uns also nicht zu kümmern. Der LoginView haben wir gerade mitgeteilt, wo diese gerendert werden soll:

```django
<!--login.html-->
<form method="POST">
  {% csrf_token %} 
  {{ form }}
  <button type="submit">Login</button>
</form>
```

![](assets/2024-03-13-14-52-15-image.png)

Den Redirect nach erfolgreichem Login können wir in `settings.py` unserer Main-App angeben:

```py
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/5.0/howto/static-files/

STATIC_URL = 'static/'
LOGIN_REDIRECT_URL = 'films:index'
```

Nach erfolgreicher Registrierung soll der User nun gleich zur Login-Seite weitergeleitet werden.

Wir ändern `views.py` der User-App:

```py
messages.success(request, f'Willkommen {username}! Du bist nun eingeloggt.')
form.save()
return redirect('login')
```

!!! note "Verständnis"

    Warum wird die Bestätigungs-Message nicht auf der Login-Seite angezeigt? 

## Logout

![](assets/2024-03-13-16-46-22-image.png)

!!! bug "Depreciated"

    Der Logout funktioniert mit Aufruf der URL nicht, da die GET-Methode für LogoutView aus Sicherheitsgründen 2023 abgestellt wurde.

Wir werden **im nächsten Abschnitt** einen Logout-Button einbauen, der mit `POST` zugreift.

## Login/Logout NAV

In der Nav möchten wir eine dynamische Login/Logout-Möglichkeit abhängig vom jeweiligen Status des Users machen.

Django stellt uns Informationen zum Userstatus zur Verfügung, die wir für diese Funktionalität nutzen können: `user.is_authenticated`

Wir nutzen das direkt in `base.html` in der navbar-Section:

```django
<li class="nav-item">
            {% if user.is_authenticated %}
              <form action="{% url 'logout' %}" method="post">
                {% csrf_token %}
                <button type="submit">Logout</button>
              </form>
            {% else %}
              <a href="{% url 'login' %}">Login</a>
            {% endif %}
          </li>
```

Wie oben erwähnt müssen wir für Logout zwangsläufig mit POST zugreifen, weshalb hier auch ein Button angezeigt wird.

![](assets/2024-03-20-12-08-08-image.png)

![](assets/2024-03-20-12-08-26-image.png)

Mit Bootstrap können wir den Button etwas gefälliger gestalten:

```html
<button class="btn btn-link" type="submit">Logout</button>
```
